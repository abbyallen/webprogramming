(function(){
    //var vs let vs const
    let x =1;

    if(x === 1){
        let x = 2;      //changes by scope
        console.log("inside scope");
        console.log(`x = ${x}`); // ` = allows you to put varis in a string
    }

    console.log("outside scope");
    console.log(`x = ${x}`);

    var y = 1;
    if(y === 1){
        var y = 2;      //overrides previous values
        console.log("inside scope");
        console.log(`y = ${y}`);
    }

    console.log("outside scope");
    console.log(`y = ${y}`);

    const z = 1;
    
    console.log(z);
    
    try {
        z = 3;
    } catch(error){
        console.log(error)
    }

    //truthy / falsy
    console.log("false == 'false'", false == 'false');
    console.log("false === 'false'", false === 'false');
    console.log("false == '0'", false == '0');
    console.log("false === '0'", false === '0');

    console.log("' \\t\\r\\n ' == 0", ' \t\r\n ' == 0)
    console.log("' \\t\\r\\n ' === 0", ' \t\r\n ' === 0)

    let array = [3, 4, 2, 5, 6, 7, 8, 9];

    // for (let index = 0; index < array.length; index++){
    //     const element = array[index];
    //     console.log(element);
    // }

    console.log(array)
    array.forEach(function(value, index){
        console.log("inside foreach val: ", value, "indx:", index)
    })


})(); //(); = invoked (called immediately)