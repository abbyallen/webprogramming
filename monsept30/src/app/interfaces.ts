export interface ISocketMessage {
    numUser?: number;   // ? = optional
    username?: string;
    message?: string;
    isLog?: boolean;
    color?: string;
    fade?: boolean;
}