import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ISocketMessage } from '../interfaces';

@Component({
  selector: '[app-message]', //makes it attribute
  templateUrl: './message.component.html',
  //ticks let you do multiple lines
  styles: [`  
    .username{
      font-weight: 700;
      overflow: hidden;
      padding-right: 15px;
      text-align: right;
    }
  `]
})
export class MessageComponent implements OnInit {
  @Input('app-message') public appMessage: ISocketMessage;  //binds app-message to html
  @HostBinding('class.log') public isLog: boolean;
  @HostBinding('class.message')public isMessage: boolean;
  public color: string;
  public message: string;
  public username: string;

  constructor() { }

  ngOnInit() {
    this.isLog = this.appMessage.isLog;
    this.isMessage = !this.appMessage.isLog;
    this.color = this.appMessage.color;
    this.message = this.appMessage.message;
    this.username = this.appMessage.username;
  }

}
